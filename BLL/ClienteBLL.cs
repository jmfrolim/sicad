﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using DAL;
using DTO;

namespace BLL
{
    public class ClienteBLL
    {
        Acesso acesso = new Acesso();

        public string Inserir(Cliente cliente)
        {
            try
            {
                acesso.LimparParametros();
                acesso.AdicionarParametos("@Nome", cliente.Nome);
                acesso.AdicionarParametos("DataNascimento", cliente.DataNascimento);
                acesso.AdicionarParametos("@Sexo", cliente.Sexo);
                acesso.AdicionarParametos("LimiteCompra", cliente.LimiteDeCompra);
                string idCliente = acesso.ExecutarManipulacao(CommandType.StoredProcedure, "spInserir").ToString();

                return idCliente;

            }
            catch (Exception exception)
            {

                return exception.Message;
            }
           
        }

        public string Alterar(Cliente cliente)
        {
            try
            {
                acesso.LimparParametros();
                acesso.AdicionarParametos("@ID", cliente.IdCliente);
                acesso.AdicionarParametos("@Nome", cliente.Nome);
                acesso.AdicionarParametos("@DataNascimento", cliente.DataNascimento);
                acesso.AdicionarParametos("@Sexo", cliente.Sexo);
                acesso.AdicionarParametos("@LimiteCompra", cliente.LimiteDeCompra);

                string Idcliente = acesso.ExecutarManipulacao(CommandType.StoredProcedure, "spAlterar").ToString();

                return Idcliente;
            }
            catch (Exception excepcion)
            {

                return excepcion.Message;
            }

        }

        public string Excluir(Cliente cliente)
        {

            try
            {
                acesso.LimparParametros();
                acesso.AdicionarParametos("@ID", cliente.IdCliente);
                string IdCliente = acesso.ExecutarManipulacao(CommandType.StoredProcedure, "spExcluir").ToString();

                return IdCliente;
            }
            catch (Exception ex)
            {

               return ex.Message;
            }
           
        }

        public ClienteColecao ConsultaPorNome(string nome)
        {

            try
            {
                ClienteColecao clientCollect = new ClienteColecao();

                acesso.LimparParametros();
                acesso.AdicionarParametos("@Nome", nome);
                DataTable dataTableCli = acesso.ExecutaConsulta(CommandType.StoredProcedure, "spConsultaNome");

                foreach (DataRow linha in dataTableCli.Rows)
                {
                    Cliente cliente = new Cliente();
                    cliente.IdCliente = Convert.ToInt32(linha["Id"]);
                    cliente.Nome = Convert.ToString(linha["Nome"]);
                    cliente.DataNascimento = Convert.ToDateTime(linha["DataNascimento"]);
                    cliente.Sexo = Convert.ToBoolean(linha["Sexo"]);
                    cliente.LimiteDeCompra = Convert.ToDecimal(linha["LimiteCompra"]);

                    clientCollect.Add(cliente);
                }

                return clientCollect;

            }
            catch (Exception ex)
            {

                throw new Exception("Não foi possivel consultar o cliente por nome. Detalhes:  "+ ex.Message);
            }
           
        }

        public ClienteColecao ConsultaPorId(int id)
        {
            try
            {

                ClienteColecao clientCollect = new ClienteColecao();

                acesso.LimparParametros();
                acesso.AdicionarParametos("@ID", id);
                DataTable dataTableCli = acesso.ExecutaConsulta(CommandType.StoredProcedure, "spConsultaNome");

                foreach (DataRow linha in dataTableCli.Rows)
                {
                    Cliente cliente = new Cliente();
                    cliente.IdCliente = Convert.ToInt32(linha["Id"]);
                    cliente.Nome = Convert.ToString(linha["Nome"]);
                    cliente.DataNascimento = Convert.ToDateTime(linha["DataNascimento"]);
                    cliente.Sexo = Convert.ToBoolean(linha["Sexo"]);
                    cliente.LimiteDeCompra = Convert.ToDecimal(linha["LimiteCompra"]);

                    clientCollect.Add(cliente);
                }

                return clientCollect;
            }
            catch (Exception ex)
            {

                throw new Exception("Não foi possivel consultar o cliente por nome. Detalhes:  " + ex.Message);
            }
        }
    }

}
