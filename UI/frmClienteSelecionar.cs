﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;


namespace UI
{
    public partial class frmClienteSelecionar : Form
    {
        public frmClienteSelecionar()
        {
            InitializeComponent();
            dataGridViewPrincipal.AutoGenerateColumns = false;
        }

        private void frmClienteSelecionar_Load(object sender, EventArgs e)
        {

        }

        private void buttonInserir_Click(object sender, EventArgs e)
        {

        }

        private void buttonAlterar_Click(object sender, EventArgs e)
        {

        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {

        }

        private void buttonSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonPesquisar_Click(object sender, EventArgs e)
        {

            AtualizaGrid();
        }

        private void AtualizaGrid()
        {
            ClienteBLL clienteBLL = new ClienteBLL();
            //clienteBLL.ConsultaPorNome(textBoxPesquisa.Text);

            ClienteColecao clienteColecao = new ClienteColecao();
            clienteColecao = clienteBLL.ConsultaPorNome(textBoxPesquisa.Text);
            dataGridViewPrincipal.DataSource = null;
            dataGridViewPrincipal.DataSource = clienteColecao;
            dataGridViewPrincipal.Update();
            dataGridViewPrincipal.Refresh();
        }

        private void dataGridViewPrincipal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonExcluir_Click(object sender, EventArgs e)
        {
            if (dataGridViewPrincipal.SelectedRows.Count == 0)
            {
                MessageBox.Show("Selecione um Cliente");
                return;
            }

            DialogResult resultado = MessageBox.Show("Tem Certeza?","Pergunta",MessageBoxButtons.YesNo,MessageBoxIcon.Question);

            if (resultado == DialogResult.No)
            {
                return;
            }
            Cliente cliSelecionado = (dataGridViewPrincipal.SelectedRows[0].DataBoundItem as Cliente);

            ClienteBLL clienteBLL = new ClienteBLL();
            string retorno = clienteBLL.Excluir(cliSelecionado);

            try
            {
                int IdCliente = Convert.ToInt32(retorno);
                MessageBox.Show("Cliente Excluido com Sucesso!" , "Aviso",MessageBoxButtons.OK,MessageBoxIcon.Information);
                AtualizaGrid();
            }
            catch
            {
                MessageBox.Show("Não foi Possivel Excluir o Cliente Selecionado" + retorno , "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ;
            }
        }
    }
}
